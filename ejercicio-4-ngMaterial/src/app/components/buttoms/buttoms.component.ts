import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-buttoms',
  templateUrl: './buttoms.component.html',
  styleUrls: ['./buttoms.component.css']
})
export class ButtomsComponent implements OnInit {

  caja_texto: string[] = []
  caja_texto1: string[] = []
  form!: FormGroup;

  constructor(private fb: FormBuilder) {
    this.Buttoms();

  }

  Buttoms(): void {
    this.form = this.fb.group({
      cajaGuargar: [''],
      caja: this.fb.array([[]]),

    })

  }

  ngOnInit(): void {
  }

  get caja_contenido() {
    return this.form.get('caja') as FormArray
  }
  agregar(): void {
    this.caja_contenido.push(this.fb.control('', Validators.required))
  }
  borrar(i: number): void {
    this.caja_contenido.removeAt(i);
  }
  limpiar(): void {
    this.caja_texto = ['']
    this.form.reset
  }
  guardar(): void {
    console.log('guardar');
    this.caja_texto = this.form.value.caja
  }

  limpiarCaja(): void {
    this.caja_texto = ['']
  }

}
